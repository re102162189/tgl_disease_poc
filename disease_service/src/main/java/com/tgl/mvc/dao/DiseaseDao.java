package com.tgl.mvc.dao;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import com.tgl.mvc.model.Disease;

public interface DiseaseDao extends MongoRepository<Disease, String> {
  
  Disease findByDiseaseId(final String diseaseId);
  
  @Query("{'notices.noticeId': ?0}")
  List<Disease> findByNoticeId(final String noticeId);
}
