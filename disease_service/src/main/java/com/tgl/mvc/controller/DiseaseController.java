package com.tgl.mvc.controller;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tgl.mvc.api.output.DiseaseIdResult;
import com.tgl.mvc.api.output.DiseaseInfoResult;
import com.tgl.mvc.service.DiseaseService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Validated
@RestController
@RequestMapping(value = "/api/v1/disease")
public class DiseaseController {
  
  private static final String MSG_200_DISEASEID = "Successfully retrieved disease id list";
  private static final String MSG_200_CHECKIFNO = "Successfully retrieved disease related questionnaires and health examinations";
   
  @Autowired
  private DiseaseService diseaseService;
  
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = MSG_200_CHECKIFNO) })
  @GetMapping(value = "/diseaseIds/{diseaseIds}")
  public ResponseEntity<DiseaseInfoResult> getDiseaseInfo(
      @PathVariable(name = "diseaseIds", required = true) 
      @Valid 
      @Size(min = 1, max = 12, message = "disease Id too long, size shoudld be less than 12")
      List<String> diseaseIds,
      @RequestParam(name = "info", required = true) 
      DiseaseService.Info info) {
    return new ResponseEntity<>(diseaseService.getDiseaseInfo(diseaseIds, info), HttpStatus.OK);
  }

  @ApiResponses(value = {
      @ApiResponse(code = 200, message = MSG_200_DISEASEID) })
  @GetMapping(value = "/descriptions/{descriptions}")
  public ResponseEntity<DiseaseIdResult> getDiseaseIds(
      @PathVariable(name = "descriptions", required = true)
      @Valid 
      @Size(min = 1, max = 12, message = "descriptions too long, size shoudld be less than 12")
      List<String> descriptions) {
    DiseaseIdResult results = diseaseService.getDiseaseIds(descriptions);
    return new ResponseEntity<>(results, HttpStatus.OK);
  }
}
