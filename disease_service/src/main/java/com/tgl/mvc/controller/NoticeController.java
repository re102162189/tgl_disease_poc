package com.tgl.mvc.controller;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tgl.mvc.api.output.NoticeDiseaseResult;
import com.tgl.mvc.service.DiseaseService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Validated
@RestController
@RequestMapping(value = "/api/v1/notice")
public class NoticeController {

  private static final String MSG_200_NOTICEMATCHS = "Successfully retrieve notice id & disease code match/fail list";
  
  @Autowired
  private DiseaseService diseaseService;
  
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = MSG_200_NOTICEMATCHS) })
  @GetMapping(value = "/noticeIds/{noticeIds}/diseaseIds/{diseaseIds}")
  public ResponseEntity<NoticeDiseaseResult> match(
      @PathVariable(name = "noticeIds", required = true) 
      @Valid 
      @Size(min = 1, max = 12, message = "notice Id too long, size shoudld be less than 12")
      List<String> noticeIds,
      @PathVariable(name = "diseaseIds", required = true) 
      @Valid 
      @Size(min = 1, max = 12, message = "disease Id too long, size shoudld be less than 12")
      List<String> diseaseIds) {
    NoticeDiseaseResult results = diseaseService.noticeDiseaseMatch(noticeIds, diseaseIds);
    return new ResponseEntity<>(results, HttpStatus.OK);
  }
}
