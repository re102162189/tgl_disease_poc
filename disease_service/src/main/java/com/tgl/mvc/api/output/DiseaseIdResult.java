package com.tgl.mvc.api.output;

import java.util.Map;
import io.swagger.annotations.ApiModelProperty;

public class DiseaseIdResult extends BaseResult {
  
  @ApiModelProperty(notes = "input disease description and disease id mapping")
  private Map<String, String> matchMap;

  public Map<String, String> getMatchMap() {
    return matchMap;
  }

  public void setMatchMap(Map<String, String> matchMap) {
    this.matchMap = matchMap;
  }
}
