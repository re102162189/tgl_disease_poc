package com.tgl.mvc.api.output;

import io.swagger.annotations.ApiModelProperty;

public abstract class InfoResult {
  
  @ApiModelProperty(notes = "info code")
  private String code;
  
  @ApiModelProperty(notes = "info description")
  private String description;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
