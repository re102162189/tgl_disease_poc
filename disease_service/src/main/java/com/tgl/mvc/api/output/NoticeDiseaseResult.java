package com.tgl.mvc.api.output;

import java.util.List;
import java.util.Map;
import io.swagger.annotations.ApiModelProperty;

public class NoticeDiseaseResult extends BaseResult {

  @ApiModelProperty(notes = "notice id and disease id mapping")
  private Map<String, List<String>> matchMap;

  @ApiModelProperty(notes = "suggest notice id for disease id")
  private Map<String, List<String>> suggestionNoticeMap;

  public Map<String, List<String>> getMatchMap() {
    return matchMap;
  }

  public void setMatchMap(Map<String, List<String>> matchMap) {
    this.matchMap = matchMap;
  }

  public Map<String, List<String>> getSuggestionNoticeMap() {
    return suggestionNoticeMap;
  }

  public void setSuggestionNoticeMap(Map<String, List<String>> suggestionNoticeMap) {
    this.suggestionNoticeMap = suggestionNoticeMap;
  }
}
