package com.tgl.mvc.api.output;

import java.util.List;
import java.util.Map;
import io.swagger.annotations.ApiModelProperty;

public abstract class BaseResult {

  @ApiModelProperty(notes = "failed match list")
  private Map<String, List<String>> failMatch;

  public Map<String, List<String>> getFailMatch() {
    return failMatch;
  }

  public void setFailMatch(Map<String, List<String>> failMatch) {
    this.failMatch = failMatch;
  }
}
