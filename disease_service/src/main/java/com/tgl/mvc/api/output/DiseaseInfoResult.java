package com.tgl.mvc.api.output;

import java.util.List;
import java.util.Map;
import io.swagger.annotations.ApiModelProperty;

public class DiseaseInfoResult extends BaseResult {
  
  @ApiModelProperty(notes = "disease info results")
  private Map<String, List<InfoResult>> resultMap;

  public Map<String, List<InfoResult>> getResultMap() {
    return resultMap;
  }

  public void setResultMap(Map<String, List<InfoResult>> resultMap) {
    this.resultMap = resultMap;
  }
}
