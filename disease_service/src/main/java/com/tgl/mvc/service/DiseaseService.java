package com.tgl.mvc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgl.mvc.api.output.DiseaseIdResult;
import com.tgl.mvc.api.output.DiseaseInfoResult;
import com.tgl.mvc.api.output.NoticeDiseaseResult;
import com.tgl.mvc.dao.DiseaseDao;
import com.tgl.mvc.model.Disease;
import com.tgl.mvc.model.Examination;
import com.tgl.mvc.model.Notice;
import com.tgl.mvc.model.Questionnaire;

@Service
public class DiseaseService {

  private static final String NOTICE_KEY = "noticeId";
  private static final String DISEASE_KEY = "diseaseId";
  private static final String DESCRIPTION_KEY = "description";

  public enum Info {
    QUESTIONNAIRE, EXAMINATION
  }

  @Autowired
  private DiseaseDao diseaseDao;

  public DiseaseIdResult getDiseaseIds(List<String> descriptions) {

    Map<String, List<String>> failMatchMap = new HashMap<>();
    failMatchMap.put(DESCRIPTION_KEY, new ArrayList<>());

    DiseaseIdResult results = new DiseaseIdResult();
    results.setFailMatch(failMatchMap);
    //
    // TODO should handle free text input
    //
    failMatchMap.put(DESCRIPTION_KEY, descriptions);
    return results;
  }

  public NoticeDiseaseResult noticeDiseaseMatch(List<String> noticeIds, List<String> diseaseIds) {

    Map<String, List<String>> failMatchMap = new HashMap<>();
    failMatchMap.put(NOTICE_KEY, new ArrayList<>());
    failMatchMap.put(DISEASE_KEY, new ArrayList<>());

    NoticeDiseaseResult results = new NoticeDiseaseResult();
    results.setFailMatch(failMatchMap);
    results.setMatchMap(new HashMap<>());
    results.setSuggestionNoticeMap(new HashMap<>());

    // flag false for all unchecked input diseaseIds
    Map<String, Boolean> diseaseMap = new HashMap<>();
    for (String diseaseId : diseaseIds) {
      diseaseMap.put(diseaseId, false);
    }

    for (String noticeId : noticeIds) {
      List<Disease> diseases = diseaseDao.findByNoticeId(noticeId);

      if (diseases == null || diseases.isEmpty()) {
        results.getFailMatch().get(NOTICE_KEY).add(noticeId);
        continue;
      }

      List<String> matchDiseases;
      if (results.getMatchMap().containsKey(noticeId)) {
        matchDiseases = results.getMatchMap().get(noticeId);
      } else {
        matchDiseases = new ArrayList<>();
        results.getMatchMap().put(noticeId, matchDiseases);
      }

      // check is there any match disease id retrieved by notice id, if yes then flag true
      for (Disease disease : diseases) {
        String diseaseId = disease.getDiseaseId();
        if (diseaseMap.containsKey(diseaseId)) {
          matchDiseases.add(diseaseId);
          diseaseMap.put(diseaseId, true);
        }
      }

      // if notice id without matched disease id, then remove it from match map
      if (matchDiseases.isEmpty()) {
        results.getMatchMap().remove(noticeId);
        results.getFailMatch().get(NOTICE_KEY).add(noticeId);
      }
    }

    // check if there is still false flag disease ids &
    // suggest if there is notice id for flag disease ids
    for (Entry<String, Boolean> diseaseEntry : diseaseMap.entrySet()) {

      boolean isMatch = diseaseEntry.getValue();
      if (isMatch) {
        continue;
      }

      String diseaseId = diseaseEntry.getKey();
      results.getFailMatch().get(DISEASE_KEY).add(diseaseId);
      Disease disease = diseaseDao.findByDiseaseId(diseaseId);
      if (disease.getNotices() != null && !disease.getNotices().isEmpty()) {

        List<String> noticeList = new ArrayList<>();
        for (Notice notice : disease.getNotices()) {
          noticeList.add(notice.getNoticeId());
        }
        results.getSuggestionNoticeMap().put(diseaseId, noticeList);
      }
    }

    // clear empty fail list
    if (results.getFailMatch().get(NOTICE_KEY).isEmpty()) {
      results.getFailMatch().remove(NOTICE_KEY);
    }

    if (results.getFailMatch().get(DISEASE_KEY).isEmpty()) {
      results.getFailMatch().remove(DISEASE_KEY);
    }

    return results;
  }

  public DiseaseInfoResult getDiseaseInfo(List<String> diseaseIds, Info info) {

    Map<String, List<String>> failMatchMap = new HashMap<>();
    failMatchMap.put(DISEASE_KEY, new ArrayList<>());

    DiseaseInfoResult result = new DiseaseInfoResult();
    result.setResultMap(new HashMap<>());
    result.setFailMatch(failMatchMap);

    for (String diseaseId : diseaseIds) {
      Disease disease = diseaseDao.findByDiseaseId(diseaseId);

      if(disease == null) {
        result.getFailMatch().get(DISEASE_KEY).add(diseaseId);
        continue;
      }
      
      switch (info) {
        case QUESTIONNAIRE:
          if (disease.getQuestionnaires() != null) {
            result.getResultMap().put(diseaseId, new ArrayList<>());
            List<Questionnaire> questionnaires = disease.getQuestionnaires();
            for (Questionnaire questionnaire : questionnaires) {
              result.getResultMap().get(diseaseId).add(questionnaire);
            }
          } else {
            result.getFailMatch().get(DISEASE_KEY).add(diseaseId);
          }
          break;
        case EXAMINATION:
          if (disease.getExaminations() != null) {
            result.getResultMap().put(diseaseId, new ArrayList<>());
            List<Examination> examinations = disease.getExaminations();
            for (Examination examination : examinations) {
              result.getResultMap().get(diseaseId).add(examination);
            }
          } else {
            result.getFailMatch().get(DISEASE_KEY).add(diseaseId);
          }
          break;
        default:
          result.getFailMatch().get(DISEASE_KEY).add(diseaseId);
          break;
      }
    }

    return result;
  }
}
