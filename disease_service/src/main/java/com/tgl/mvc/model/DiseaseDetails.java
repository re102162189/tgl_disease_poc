package com.tgl.mvc.model;

import java.util.List;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModelProperty;

public class DiseaseDetails {

  @ApiModelProperty(notes = "disease name / injured area details")
  @Size(min = 1, max = 60, message = "description too long, size shoudld be less than 60")
  private List<String> description;

  public List<String> getDescription() {
    return description;
  }

  public void setDescription(List<String> description) {
    this.description = description;
  }
}
