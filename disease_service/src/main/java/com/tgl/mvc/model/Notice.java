package com.tgl.mvc.model;

import io.swagger.annotations.ApiModelProperty;

public class Notice {
  
  @ApiModelProperty(notes = "notice id list")
  private String noticeId;
  
  @ApiModelProperty(notes = "notice description")
  private String description;

  public String getNoticeId() {
    return noticeId;
  }

  public void setNoticeId(String noticeId) {
    this.noticeId = noticeId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
