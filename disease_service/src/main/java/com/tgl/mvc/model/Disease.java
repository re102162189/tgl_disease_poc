package com.tgl.mvc.model;

import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;
import io.swagger.annotations.ApiModelProperty;

@Document(collection = "disease")
public class Disease {

  @ApiModelProperty(notes = "disease ID")
  private String diseaseId;

  @ApiModelProperty(notes = "disease related questionnaire")
  private List<Questionnaire> questionnaires;

  @ApiModelProperty(notes = "disease related health examinations")
  private List<Examination> examinations;

  @ApiModelProperty(notes = "disease related notice")
  private List<Notice> notices;

  public String getDiseaseId() {
    return diseaseId;
  }

  public void setDiseaseId(String diseaseId) {
    this.diseaseId = diseaseId;
  }

  public List<Questionnaire> getQuestionnaires() {
    return questionnaires;
  }

  public void setQuestionnaires(List<Questionnaire> questionnaires) {
    this.questionnaires = questionnaires;
  }

  public List<Examination> getExaminations() {
    return examinations;
  }

  public void setExaminations(List<Examination> examinations) {
    this.examinations = examinations;
  }

  public List<Notice> getNotices() {
    return notices;
  }

  public void setNotices(List<Notice> notices) {
    this.notices = notices;
  }
}
