package com.tgl.data;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import com.tgl.data.model.policy.Disease;
import com.tgl.data.model.policy.PolicyNotice;

public class PolicyMapping {

  private static final String MPOS_POLICY_FILE = "mpos_policy";

  private static final String EBAO_POLICY_FILE = "ebao_policy.csv";

  private static final String MONGODB = "mongodb://localhost:27017";

  private static final String DATABASE = "insurance";

  private static final String COLLECTION = "policy";

  public static void main(String[] args) {
    Map<String, PolicyNotice> policyMap = paredFiles();
    initMongoData(policyMap);
  }

  private static Map<String, PolicyNotice> paredFiles() {
    ObjectMapper objMapper = new ObjectMapper();
    Map<String, PolicyNotice> policyMap = new HashMap<>();
    ClassLoader classLoader = PolicyMapping.class.getClassLoader();
    URL resource = classLoader.getResource(MPOS_POLICY_FILE);

    try (BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()))) {
      String line;
      while ((line = reader.readLine()) != null) {
        PolicyNotice policyNotice = objMapper.readValue(line, PolicyNotice.class);
        policyNotice.setSource("mPos");
        policyMap.put(policyNotice.getPolicyId(), policyNotice);
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }

    System.out.println(MPOS_POLICY_FILE + " parsed finished");

    resource = classLoader.getResource(EBAO_POLICY_FILE);

    try (BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()))) {
      String line;
      String[] data;
      while ((line = reader.readLine()) != null) {
        //
        // 0: 保單號碼
        // 1: 要保書狀態
        // 2: 保單狀況
        // 3: 告知疾病代碼
        //
        data = line.split(",");
        String policyId = data[0];
        String applicationStatus = data[1];
        String diseaseContents = data[3]; // 9020001-textextext／9990000-textextext
        List<Disease> diseaseIds = new ArrayList<>();

        String[] items = diseaseContents.trim().split("／");
        for (String item : items) {
          String[] contents = item.trim().split("-");
          Disease diseaseId = new Disease();
          diseaseId.setDiseaseId(contents[0]);
          diseaseIds.add(diseaseId);
        }

        PolicyNotice policyNotice;
        if (policyMap.containsKey(policyId)) {
          policyNotice = policyMap.get(policyId);
          policyNotice.setSource("eBao/mPos");
        } else {
          policyNotice = new PolicyNotice();
          policyNotice.setSource("eBao");
          policyMap.put(policyId, policyNotice);
        }
        policyNotice.setApplicationStatus(applicationStatus);
        policyNotice.setDiseaseIds(diseaseIds);
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }

    System.out.println(EBAO_POLICY_FILE + " parsed finished");

    return policyMap;
  }

  private static void initMongoData(Map<String, PolicyNotice> policyMap) {
    CodecRegistry pojoCodecRegistry =
        fromProviders(PojoCodecProvider.builder().automatic(true).build());
    CodecRegistry codecRegistry =
        fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);

    ConnectionString connectionString = new ConnectionString(MONGODB);
    MongoClientSettings clientSettings = MongoClientSettings.builder()
        .applyConnectionString(connectionString).codecRegistry(codecRegistry).build();

    try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
      MongoDatabase database = mongoClient.getDatabase(DATABASE);
      MongoCollection<PolicyNotice> collection =
          database.getCollection(COLLECTION, PolicyNotice.class);
      collection.drop();
      database.createCollection(COLLECTION);
      
      List<PolicyNotice> policyNotices = new ArrayList<>(policyMap.values());
      collection.insertMany(policyNotices);
      collection.createIndex(Indexes.ascending("policyId"));
      collection.createIndex(Indexes.ascending("diseaseIds.diseaseId"));
    }
  }
}
