package com.tgl.data;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.ne;
import static com.mongodb.client.model.Filters.or;
import static com.mongodb.client.model.Filters.where;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.BsonNull;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.tgl.data.model.policy.PolicyNotice;

public class PolicyExport {

  private static final String MONGODB = "mongodb://localhost:27017";

  private static final String DATABASE = "insurance";

  private static final String COLLECTION = "policy";

  private static final String EXPORT_PATH = "/Users/kitechen/Downloads/export.xlsx";

  private static final String EMPTY_STRING = "none";

  private enum EXCEL_TITLE {
    POLICY, NOTICES, NOTES, DISEASES, APPLICATION_STATUS
  }

  public static void main(String[] args) {

    CodecRegistry pojoCodecRegistry =
        fromProviders(PojoCodecProvider.builder().automatic(true).build());
    CodecRegistry codecRegistry =
        fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);

    ConnectionString connectionString = new ConnectionString(MONGODB);
    MongoClientSettings clientSettings = MongoClientSettings.builder()
        .applyConnectionString(connectionString).codecRegistry(codecRegistry).build();

    List<PolicyNotice> policyNotices = new ArrayList<>();

    try (MongoClient mongoClient = MongoClients.create(clientSettings)) {

      MongoDatabase database = mongoClient.getDatabase(DATABASE);
      MongoCollection<PolicyNotice> collection =
          database.getCollection(COLLECTION, PolicyNotice.class);

      Bson bson = and(Arrays.asList(
          or(Arrays.asList(
              and(ne("diseaseIds.diseaseId", "9010001"), where("this.diseaseIds.length > 1")),
              and(ne("diseaseIds.diseaseId", "9020001"), where("this.diseaseIds.length > 1")),
              and(ne("diseaseIds.diseaseId", "9030001"), where("this.diseaseIds.length > 1")))),
          ne("notes", new BsonNull())));

      FindIterable<PolicyNotice> documents = collection.find(bson);
      for (PolicyNotice policyNotice : documents) {
        policyNotices.add(policyNotice);
      }
    }

    try (FileOutputStream outputStream = new FileOutputStream(EXPORT_PATH);
        Workbook workbook = new XSSFWorkbook()) {
      Sheet sheet = workbook.createSheet(COLLECTION);

      int rowNum = 0;
      Row titleRow = sheet.createRow(rowNum++);

      int titleNum = 0;
      for (EXCEL_TITLE title : EXCEL_TITLE.values()) {
        Cell titleCell = titleRow.createCell(titleNum++);
        titleCell.setCellValue(title.name());
      }

      ObjectMapper objMapper = new ObjectMapper();
      for (PolicyNotice policyNotice : policyNotices) {
        Row row = sheet.createRow(rowNum++);
        int cellNum = 0;
        for (EXCEL_TITLE title : EXCEL_TITLE.values()) {
          Cell cell = row.createCell(cellNum++);
          cell.setCellValue(EMPTY_STRING);
          switch (title) {
            case POLICY:
              cell.setCellValue((String) policyNotice.getPolicyId());
              break;
            case NOTICES:
              if (policyNotice.getNotices() != null) {
                cell.setCellValue(
                    objMapper.writeValueAsString(policyNotice.getNotices()));
              }
              break;
            case NOTES:
              if (policyNotice.getNotes() != null) {
                cell.setCellValue(objMapper.writeValueAsString(policyNotice.getNotes()));
              }
              break;
            case DISEASES:
              if (policyNotice.getDiseaseIds() != null) {
                cell.setCellValue(objMapper.writeValueAsString(policyNotice.getDiseaseIds()));
              }
              break;
            case APPLICATION_STATUS:
              if (!"".equals(policyNotice.getApplicationStatus())) {
                cell.setCellValue(policyNotice.getApplicationStatus());
              }
              break;
            default:
              break;
          }
        }
      }

      workbook.write(outputStream);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
