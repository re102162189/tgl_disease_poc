package com.tgl.data.model.disease;

import java.util.List;
import org.bson.codecs.pojo.annotations.BsonProperty;

public class Disease {

  @BsonProperty(value = "diseaseId")
  private String diseaseId;

  @BsonProperty(value = "questionnaires")
  private List<Questionnaire> questionnaires;

  @BsonProperty(value = "examinations")
  private List<Examination> examinations;
  
  @BsonProperty(value = "notices")
  private List<Notice> notices;

  public String getDiseaseId() {
    return diseaseId;
  }

  public void setDiseaseId(String diseaseId) {
    this.diseaseId = diseaseId;
  }

  public List<Questionnaire> getQuestionnaires() {
    return questionnaires;
  }

  public void setQuestionnaires(List<Questionnaire> questionnaires) {
    this.questionnaires = questionnaires;
  }

  public List<Examination> getExaminations() {
    return examinations;
  }

  public void setExaminations(List<Examination> examinations) {
    this.examinations = examinations;
  }

  public List<Notice> getNotices() {
    return notices;
  }

  public void setNotices(List<Notice> notices) {
    this.notices = notices;
  }
}
