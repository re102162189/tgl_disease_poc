package com.tgl.data.model.policy;

import org.bson.codecs.pojo.annotations.BsonProperty;

public class Disease {
  
  @BsonProperty(value = "diseaseId")
  private String diseaseId;

  public String getDiseaseId() {
    return diseaseId;
  }

  public void setDiseaseId(String diseaseId) {
    this.diseaseId = diseaseId;
  }
}
