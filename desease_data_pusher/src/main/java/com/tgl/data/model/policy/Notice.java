package com.tgl.data.model.policy;

import org.bson.codecs.pojo.annotations.BsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Notice {
  
  @BsonProperty(value = "noticeId")
  @JsonProperty("notifyItem")
  private String noticeId;
  
  @BsonProperty(value = "checked")
  @JsonProperty("notifyCheckedIndi")
  private String checked;

  public String getNoticeId() {
    return noticeId;
  }

  public void setNoticeId(String noticeId) {
    this.noticeId = noticeId;
  }

  public String getChecked() {
    return checked;
  }

  public void setChecked(String checked) {
    this.checked = checked;
  }
}
