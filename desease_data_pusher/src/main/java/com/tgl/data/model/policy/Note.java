package com.tgl.data.model.policy;

import org.bson.codecs.pojo.annotations.BsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Note {
  
  @BsonProperty(value = "treatmentResults")
  @JsonProperty("treatmentResults")
  private String treatmentResults;
  
  @BsonProperty(value = "hospital")
  @JsonProperty("hospital")
  private String hospital;
  
  @BsonProperty(value = "treatment")
  @JsonProperty("treatmentWay")
  private String treatment;
  
  @BsonProperty(value = "diseaseName")
  @JsonProperty("sickName")
  private String diseaseName;
  
  @BsonProperty(value = "treatmentDate")
  @JsonProperty("treatmentDate")
  private String treatmentDate;

  public String getTreatmentResults() {
    return treatmentResults;
  }

  public void setTreatmentResults(String treatmentResults) {
    this.treatmentResults = treatmentResults;
  }

  public String getHospital() {
    return hospital;
  }

  public void setHospital(String hospital) {
    this.hospital = hospital;
  }

  public String getTreatment() {
    return treatment;
  }

  public void setTreatment(String treatment) {
    this.treatment = treatment;
  }

  public String getDiseaseName() {
    return diseaseName;
  }

  public void setDiseaseName(String diseaseName) {
    this.diseaseName = diseaseName;
  }

  public String getTreatmentDate() {
    return treatmentDate;
  }

  public void setTreatmentDate(String treatmentDate) {
    this.treatmentDate = treatmentDate;
  }
}
