package com.tgl.data.model.policy;

import java.util.List;
import org.bson.codecs.pojo.annotations.BsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PolicyNotice {
  
  @BsonProperty(value = "policyId")
  @JsonProperty("id")
  private String policyId;
  
  @BsonProperty(value = "notices")
  @JsonProperty("insuredNotifyItemList")
  private List<Notice> notices;
  
  @BsonProperty(value = "notes")
  @JsonProperty("insuredNoteList")
  private List<Note> notes;
  
  @BsonProperty(value = "applicationStatus")
  @JsonProperty("applicationStatus")
  private String applicationStatus;
  
  @BsonProperty(value = "diseaseIds")
  private List<Disease> diseaseIds;
  
  @BsonProperty(value = "source")
  private String source;

  public String getPolicyId() {
    return policyId;
  }

  public void setPolicyId(String policyId) {
    this.policyId = policyId;
  }

  public List<Notice> getNotices() {
    return notices;
  }

  public void setNotices(List<Notice> notices) {
    this.notices = notices;
  }

  public List<Note> getNotes() {
    return notes;
  }

  public void setNotes(List<Note> notes) {
    this.notes = notes;
  }

  public String getApplicationStatus() {
    return applicationStatus;
  }

  public void setApplicationStatus(String applicationStatus) {
    this.applicationStatus = applicationStatus;
  }

  public List<Disease> getDiseaseIds() {
    return diseaseIds;
  }

  public void setDiseaseIds(List<Disease> diseaseIds) {
    this.diseaseIds = diseaseIds;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }
}
