package com.tgl.data;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import com.tgl.data.model.disease.Disease;
import com.tgl.data.model.disease.Examination;
import com.tgl.data.model.disease.Notice;
import com.tgl.data.model.disease.Questionnaire;

public class DiseaseMapping {

  private static final String DISEASE_CODE_FILE = "disease_code.csv";

  private static final String NOTICE_DISEASE_FILE = "notice_disease.csv";

  private static final String MONGODB = "mongodb://localhost:27017";

  private static final String DATABASE = "insurance";

  private static final String COLLECTION = "disease";

  public static void main(String[] args) {
    Map<String, Disease> diseaseMap = getDiseaseCodes();
    getNoticeWithDiseases(diseaseMap);
    initMongoData(diseaseMap);
  }
  
  private static void initMongoData(Map<String, Disease> diseaseMap) {
    
    CodecRegistry pojoCodecRegistry =
        fromProviders(PojoCodecProvider.builder().automatic(true).build());
    CodecRegistry codecRegistry =
        fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);

    ConnectionString connectionString = new ConnectionString(MONGODB);
    MongoClientSettings clientSettings = MongoClientSettings.builder()
        .applyConnectionString(connectionString).codecRegistry(codecRegistry).build();
    
    try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
      MongoDatabase database = mongoClient.getDatabase(DATABASE);
      MongoCollection<Disease> collection = database.getCollection(COLLECTION, Disease.class);
      collection.drop();
      database.createCollection(COLLECTION);

      List<Disease> diseases = new ArrayList<>(diseaseMap.values());
      collection.insertMany(diseases);
      collection.createIndex(Indexes.ascending("diseaseId"));
      collection.createIndex(Indexes.ascending("noticeId"));
    }
  }

  private static void getNoticeWithDiseases(Map<String, Disease> diseases) {
   
    ClassLoader classLoader = DiseaseMapping.class.getClassLoader();
    URL resource = classLoader.getResource(NOTICE_DISEASE_FILE);
    try (BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()))) {
      
      String line;
      String[] data;
      Map<String, String> diseaseIds = new HashMap<>(); // for skipping duplicate diseaseIds
      while ((line = reader.readLine()) != null) {
        //
        // 0: notice_id
        // 1: notice_desc
        // 2: disease_desc
        // 3: disease_id
        //
        data = line.split(",");
        String diseaseId = data[3];
        
        if(diseaseIds.containsKey(diseaseId)) {
          System.out.println("diseaseId has been recorded: " + diseaseId);
          continue;
        }
        
        if(!diseases.containsKey(diseaseId)) {
          System.err.println("diseaseId not found: " + diseaseId);
          continue;
        }
        
        Notice notice = new Notice();
        notice.setNoticeId(data[0]);
        notice.setDescription(data[1]);

        Disease disease = diseases.get(diseaseId);
        if(disease.getNotices() == null) {
          List<Notice> notices = new ArrayList<>();
          notices.add(notice);
          disease.setNotices(notices);
        } else {
          disease.getNotices().add(notice);
        }
        
        diseaseIds.put(diseaseId, diseaseId);
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
  }

  private static Map<String, Disease> getDiseaseCodes() {

    Map<String, Disease> diseases = new HashMap<>();
    ClassLoader classLoader = DiseaseMapping.class.getClassLoader();
    URL resource = classLoader.getResource(DISEASE_CODE_FILE);
    try (BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()))) { 
      String line;
      String[] data;
      while ((line = reader.readLine()) != null) {
        //
        // 0: disease_code
        // 1: disease_id
        // 2: disease_desc
        // 3: questionnaire
        // 4: examinations
        // 5: letter_txt
        //
        data = line.split(",");
        Disease disease = new Disease();
        disease.setDiseaseId(data[1]);
        
        if (data[3] != null && !"".equals(data[3])) {
          List<Questionnaire> questionnaires = new ArrayList<>();
          String[] items = data[3].trim().split("-");
          Questionnaire questionnaire = new Questionnaire();
          questionnaire.setCode(items[0]);
          questionnaire.setDescription(items[1]);
          questionnaires.add(questionnaire);
          disease.setQuestionnaires(questionnaires);
        }
        
        if (data[4] != null && !"".equals(data[4])) {
          List<Examination> examinations = new ArrayList<>();
          String[] items = data[4].trim().split("／");
          for (String item : items) {
            String[] contents = item.trim().split("-");
            Examination examination = new Examination();
            examination.setCode(contents[0]);
            examination.setDescription(contents[1]);
            examinations.add(examination);
          }
          disease.setExaminations(examinations);
        }
        diseases.put(disease.getDiseaseId(), disease);
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }   
    return diseases;
  }
}
