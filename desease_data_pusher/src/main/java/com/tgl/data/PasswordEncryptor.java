package com.tgl.data;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class PasswordEncryptor {
  
  public static void main(String[] args) {
    String password = "test";
    
    StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
    encryptor.setPassword("salt");
    encryptor.setAlgorithm("PBEWithMD5AndDES");
    String encryptedPassword = encryptor.encrypt(password);
    String decryptedPassword = encryptor.decrypt(encryptedPassword);
    
    System.out.println(encryptedPassword);
    System.out.println(decryptedPassword);
  }
}
